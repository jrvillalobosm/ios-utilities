#
#  Be sure to run `pod spec lint Logger.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

  spec.name         = "Logger"
  spec.version      = "2.2.1"
  spec.summary      = "Libreria para API de Logs basado en Log4j."

  spec.description  = <<-DESC
    Esta Libreria proporciona una API para manejo de logs
  DESC

  spec.homepage     = "https://bitbucket.org/jrvillalobosm"

  spec.license      = { :type => 'MIT', :file => 'LICENSE' }

  spec.author       = { 'jorge Villalobos' => 'jorge.r.villalobos.m@gmail.com' }

  spec.platform     = :ios, '11.0'

  spec.source       = { :git => "https://bitbucket.org/jrvillalobosm/ios-utilities.git", :tag => "Logger_2.2.1" }

  spec.source_files = 'Sources/Logger/**/*.swift'

  spec.swift_versions = ['4.2', '5.0']
  spec.frameworks   = 'UIKit'

  spec.dependency 'CommonsAssets','~> 2.2.0'
end
