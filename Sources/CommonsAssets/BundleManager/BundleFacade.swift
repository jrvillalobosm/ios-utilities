//
//  BundleFacade.swift
//
//  Created by Jorge Villalobos on 4/5/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import Foundation

public class BundleFacade {
    public static let shared: BundleFacade = BundleFacade()

    private var bundleManager = [String: BundleResource]()

    private init() { }

    public func addResourceBundle(bundleId: String, bundle: Bundle = .main) throws {
        guard bundleManager[bundleId] == nil else {
            return
        }
        let bundleWrapper = try BundleResource(name: bundleId, bundle: bundle)
        bundleManager[bundleId] = bundleWrapper
    }

    public func displayMessage(bundleId: String,
        key: String,
        returnValue: Bool = true,
        args: [CVarArg] = []
    ) -> String! {
        var message: String
        do {
            let dict = try getResourceBundle(bundleId: bundleId)
            guard let value = dict[key].string else {
                throw PlistError.KeyDoesNotExit
            }
            message = value
        } catch let error as PlistError {
            guard returnValue else {
                return nil
            }
            return error.getMessageError(bundleId, key)
        } catch {
            return nil
        }
        return message.formatter(args: args)
    }

    public func getResourceBundle(bundleId: String) throws -> BundleResource {
        guard let bundleWrapper = bundleManager[bundleId] else {
            throw PlistError.BundleDoesNotExist
        }
        return bundleWrapper
    }
}
