//
//  BundleMessage.swift
//
//  Created by Jorge Villalobos on 15/08/18.
//  Copyright © 2018 Jorge Villalobos. All rights reserved.
//

import Foundation

public enum DefaultConstants: CustomStringConvertible {
    case configuration
    case error

    public var description: String {
        switch self {
        case .configuration:
            return "DefaultConfiguration"
        case .error:
            return "DefaultErrors"
        }
    }
}

public protocol BundleMessage {
    var bundleId: String { get }
    var key: String { get }

    func getMessage(_ args: CVarArg...) -> String
    func getMessage(_ args: [CVarArg]) -> String
}

public extension BundleMessage {
    var bundleId: String {
        return DefaultConstants.configuration.description
    }

    func getMessage(_ args: CVarArg...) -> String {
        return self.getMessage(args)
    }

    func getMessage(_ args: [CVarArg] = []) -> String {
        let message: String! = BundleFacade.shared.displayMessage(bundleId: self.bundleId, key: self.key, args: args)
        return message
    }
}
