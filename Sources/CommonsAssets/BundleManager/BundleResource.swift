//
//  BundleResource.swift
//
//  Created by Jorge Villalobos on 4/5/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import Foundation

public protocol IDataResource {
    var int: Int? { get }
    var value: Any? { get }
    var bool: Bool? { get }
    var data: Data? { get }
    var date: Date? { get }
    var float: Float? { get }
    var array: NSArray? { get }
    var double: Double? { get }
    var string: String? { get }
    var number: NSNumber? { get }
    var dict: NSDictionary? { get }
}

public enum BundleResource {
    case dictionary(NSDictionary)
    case Array(NSArray)
    case Value(Any)
    case none

    public init(name: String, bundle: Bundle = .main) throws {
        let fileManager = FileManager.default
        guard let source = bundle.path(forResource: name, ofType: "plist") else {
            throw PlistError.FileDoesNotExist
        }
        guard fileManager.fileExists(atPath: source) else {
            throw PlistError.FileDoesNotExist
        }

        if let dict = NSDictionary(contentsOfFile: source) {
            self = .dictionary(dict)
        }
        else if let array = NSArray(contentsOfFile: source) {
            self = .Array(array)
        }
        else {
            self = .none
        }
    }
}

public extension BundleResource {
    fileprivate static func wrap(_ object: Any?) -> BundleResource {
        if let dict = object as? NSDictionary {
            return .dictionary(dict)
        }
        if let array = object as? NSArray {
            return .Array(array)
        }
        if let value = object {
            return .Value(value)
        }
        return .none
    }

    fileprivate func cast<T>() -> T? {
        switch self {
        case let .Value(value):
            return value as? T
        default:
            return nil
        }
    }
}

public extension BundleResource {
    subscript(key: String) -> BundleResource {
        switch self {
        case let .dictionary(dict):
            let v = dict.object(forKey: key)
            return BundleResource.wrap(v)
        default:
            return .none
        }
    }

    subscript(index: Int) -> BundleResource {
        switch self {
        case let .Array(array):
            if index >= 0 && index < array.count {
                return BundleResource.wrap(array[index])
            }
            return .none

        default:
            return .none
        }
    }
}

extension BundleResource: IDataResource {
    public var string: String? { return cast() }
    public var int: Int? { return cast() }
    public var double: Double? { return cast() }
    public var float: Float? { return cast() }
    public var date: Date? { return cast() }
    public var data: Data? { return cast() }
    public var number: NSNumber? { return cast() }
    public var bool: Bool? { return cast() }

    public var value: Any? {
        switch self {
        case let .Value(value):
            return value
        case let .dictionary(dict):
            return dict
        case let .Array(array):
            return array
        case .none:
            return nil
        }
    }

    public var array: NSArray? {
        switch self {
        case let .Array(array):
            return array
        default:
            return nil
        }
    }

    public var dict: NSDictionary? {
        switch self {
        case let .dictionary(dict):
            return dict
        default:
            return nil
        }
    }
}

extension BundleResource: CustomStringConvertible {
    public var description: String {
        switch self {
        case let .Array(array): return "(array \(array))"
        case let .dictionary(dict): return "(dict \(dict))"
        case let .Value(value): return "(value \(value))"
        case .none: return "(none)"
        }
    }
}
