//
//  DataTypes+Patterns.swift
//
//  Created by Jorge Villalobos on 12/12/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import Foundation

public protocol Command {
    func execute()
}
