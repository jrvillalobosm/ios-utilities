//
//  Array+Extensions.swift
//
//  Created by Jorge Villalobos on 10/16/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import Foundation

public extension Array {
    func find(filter: (Array.Iterator.Element) -> Bool) -> Element? {
        if let itemIndex = self.firstIndex(where: filter) {
            return self[itemIndex]
        }
        return nil
    }

    func insertionIndex(for elem: Element, isOrderedBefore: (Element, Element) -> Bool) -> Int {
        var lo = 0
        var hi = self.count - 1
        while lo <= hi {
            let mid = (lo + hi) / 2
            if isOrderedBefore(self[mid], elem) {
                lo = mid + 1
            } else if isOrderedBefore(elem, self[mid]) {
                hi = mid - 1
            } else {
                return mid
            }
        }
        return lo
    }
}

public extension Array where Element: Comparable {
    func containsSameElements(as other: [Element]) -> Bool {
        return self.count == other.count && self.sorted() == other.sorted()
    }
}
