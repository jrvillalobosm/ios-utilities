//
//  Bundle+Extensions.swift
//
//  Created by Jorge Villalobos on 15/08/18.
//  Copyright © 2018 Jorge Villalobos. All rights reserved.
//

import Foundation

public extension Bundle {
    var releaseVersionNumber: String? {
        return infoDictionary?["CFBundleShortVersionString"] as? String
    }
    var buildVersionNumber: String? {
        return infoDictionary?["CFBundleVersion"] as? String
    }
}
