//
//  Data+Extensions.swift
//
//  Created by Jorge Villalobos on 15/08/18.
//  Copyright © 2018 Jorge Villalobos. All rights reserved.
//

import Foundation
import UIKit

public extension Data {

    var attributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self,
                options: [NSAttributedString.DocumentReadingOptionKey
                        .documentType: NSAttributedString.DocumentType.html,
                    NSAttributedString.DocumentReadingOptionKey
                        .characterEncoding: String.Encoding.utf8.rawValue],
                documentAttributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        return nil
    }
}
