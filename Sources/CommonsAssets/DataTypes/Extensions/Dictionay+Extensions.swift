//
//  Dictionay+Extensions.swift
//
//  Created by Jorge Villalobos on 21/06/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import Foundation

public extension Dictionary {

    subscript(i: Int) -> (key: Key, value: Value) {
        get {
            return self[index(startIndex, offsetBy: i)];
        }
    }

    static func += (left: inout Dictionary, right: Dictionary) {
        for (key, value) in right {
            left[key] = value
        }
    }
}

public func + <K, V> (left: [K: V], right: [K: V]?) -> [K: V] {
    guard let right = right else { return left }
    return left.reduce(right) {
        var new = $0 as [K: V]
        new.updateValue($1.1, forKey: $1.0)
        return new
    }
}

public func += <K, V> (left: inout [K: V], right: [K: V]) {
    for (k, v) in right {
        left.updateValue(v, forKey: k)
    }
}
