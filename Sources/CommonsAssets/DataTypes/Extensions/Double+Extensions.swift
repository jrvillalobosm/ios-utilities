//
//  Double+Extensions.swift
//
//  Created by Jorge Villalobos on 15/08/18.
//  Copyright © 2018 Jorge Villalobos. All rights reserved.
//

import Foundation

public extension Double {
    func formatRounded(min: Int = 2,
        max: Int = 2,
        roundingMode: NumberFormatter.RoundingMode = .halfEven
    ) -> String {
        let number = NumberFormatter()
        number.minimumFractionDigits = min
        number.maximumFractionDigits = max
        number.roundingMode = roundingMode
        number.numberStyle = .decimal
        return number.string(for: self) ?? ""
    }

    func format(digits: Int = 1) -> String {
        return String(format: "%.\(digits)f", self)
    }
}
