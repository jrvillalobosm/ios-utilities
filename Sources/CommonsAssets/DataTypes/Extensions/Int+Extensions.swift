//
//  Int+Extensions.swift
//
//  Created by Jorge Villalobos on 03/10/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import Foundation

public enum Separator: String {
    case COMMAS = ","
    case SPACE = " "
    case DOT = "."
}

public extension Int {
    func formatterSeparator(with: Separator) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.groupingSeparator = with.rawValue
        return formatter.string(from: NSNumber(value: self))!
    }
}
