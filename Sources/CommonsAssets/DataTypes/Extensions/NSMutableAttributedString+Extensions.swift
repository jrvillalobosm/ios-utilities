//
//  NSMutableAttributedString+Extensions.swift
//
//  Created by Jorge Villalobos on 18/07/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import Foundation
import UIKit

public extension NSMutableAttributedString {

    @discardableResult func bold(_ text: String, _ ofSize: CGFloat = 17) -> NSMutableAttributedString {
        let attrs = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: ofSize)]
        let boldString = NSMutableAttributedString(string: "\(text)", attributes: attrs)
        self.append(boldString)
        return self
    }

    @discardableResult func normal(_ text: String) -> NSMutableAttributedString {
        let normal = NSAttributedString(string: text)
        self.append(normal)
        return self
    }

    @discardableResult func struck(_ text: String) -> NSMutableAttributedString {
        let attributeString = NSMutableAttributedString(string: text)
        attributeString.addAttribute(.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
        self.append(attributeString)
        return self
    }
}
