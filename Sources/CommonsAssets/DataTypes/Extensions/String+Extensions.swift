//
//  String+Extensions.swift
//
//  Created by Jorge Villalobos on 4/5/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import Foundation
import UIKit

extension StringProtocol {
    public func stringByRemovingLastComponent(withDelimiter delimiter: String) -> SubSequence? {
        guard let markerIndex = self.reversed().firstIndex(of: Character(delimiter)) else { return nil }
        let endIndex = self.index(markerIndex.base, offsetBy: -1)

        let result = self[self.startIndex..<endIndex]
        return result
    }
}

public extension String {
    var hexColor: UIColor {
        return UIColor(hexString: self)
    }

    func formatter(_ args: CVarArg...) -> String {
        guard args.count > 0 else {
            return self
        }

        return withVaList(args) { (argsListPointer) in
            NSString(format: self, arguments: argsListPointer) as String
        }
    }

    func formatter(args: [CVarArg] = []) -> String {
        guard args.count > 0 else {
            return self
        }

        return withVaList(args) { (argsListPointer) in
            NSString(format: self, arguments: argsListPointer) as String
        }
    }

    func replace(_ target: String, _ withString: String) -> String {
        return self.replacingOccurrences(of: target,
            with: withString,
            options: NSString.CompareOptions.literal,
            range: nil)
    }

    func join<S: Sequence>(_ elements: S) -> String {
        return elements.map { String(describing: $0) }.joined(separator: self)
    }

    func htmlToString() -> NSAttributedString {
        var utf8Data: Data? {
            return data(using: .utf8)
        }
        return (utf8Data?.attributedString)!
    }

    func toJson() throws -> [String: AnyObject] {
        var dict: [String: AnyObject] = Dictionary()
        let s = (self as NSString).replacingOccurrences(of: "'", with: "\"")
        if let data = s.data(using: String.Encoding.utf8) {
            dict = try JSONSerialization.jsonObject(with: data,
                options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
        }
        return dict
    }

    func pad(toWidth width: Int) -> String {
        var paddedString: String = self

        if width == 0 {
            return self
        }

        if self.count > abs(width) {
            if width < 0 {
                paddedString = String(self.suffix(abs(width)))
            } else {
                paddedString = String(self.prefix(width))
            }
        }

        if self.count < abs(width) {
            if width < 0 {
                paddedString = " ".padding(toLength: abs(width) - self.count, withPad: " ", startingAt: 0) + self
            } else {
                paddedString = self.padding(toLength: width, withPad: " ", startingAt: 0)
            }
        }

        return paddedString
    }

    func find(_ char: Character) -> Index? {
        #if swift(>=3.2)
            return self.firstIndex(of: char)
        #else
            return self.characters.index(of: char)
        #endif
    }
}
