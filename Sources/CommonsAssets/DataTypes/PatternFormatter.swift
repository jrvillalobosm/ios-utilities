//
//  PatternFormatter.swift
//
//  Created by Jorge Villalobos on 11/12/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import Foundation

@objc public final class PatternFormatter: NSObject {
    typealias FormattingClosure = (_ message: Json) throws -> String
    private var formattingClosuresSequence = [FormattingClosure]()

    public init(pattern: String) throws {
        super.init()
        let parser = PatternParser()
        self.formattingClosuresSequence = try parser.parsePattern(pattern)
    }

    public func formatter(message: Json = [:]) throws -> String {
        return try formattingClosuresSequence.reduce("") {
            (accumulatedValue, currentItem) throws in accumulatedValue + (try currentItem(message))
        }
    }

    private class PatternParser {
        private enum ParserState {
            case Text
            case Parameters
            case End
        }

        private struct ParserStatus {
            var machineState = ParserState.Text
            var charactersAccumulator = [Character]()

            func getSubPath() -> String {
                return String(charactersAccumulator)
            }
        }

        private var parserStatus = ParserStatus()
        private var parsedClosuresSequence = [FormattingClosure]()

        fileprivate func parsePattern(_ pattern: String) throws -> [FormattingClosure] {
            parsedClosuresSequence = [FormattingClosure]()
            for currentCharacter in pattern {
                switch(parserStatus.machineState) {
                case .Text where currentCharacter == "{":
                    try setParserState(.Parameters)
                case .Text:
                    parserStatus.charactersAccumulator.append(currentCharacter)
                case .Parameters where currentCharacter == "}":
                    try setParserState(.Text)
                case .Parameters where currentCharacter == "{":
                    throw PatternFormatterError.InvalidFormatSyntax
                case .Parameters:
                    parserStatus.charactersAccumulator.append(currentCharacter)
                case .End:
                    throw PatternFormatterError.InvalidFormatSyntax
                }
            }
            try setParserState(.End)
            return parsedClosuresSequence
        }

        private func setParserState(_ newState: ParserState) throws {
            switch(parserStatus.machineState) {
            case .Text where parserStatus.charactersAccumulator.count > 0:
                let parsedString = String(parserStatus.charactersAccumulator)
                if(!parsedString.isEmpty) {
                    parsedClosuresSequence.append({ (_) in return parsedString })
                    parserStatus.charactersAccumulator.removeAll()
                }
            case .Parameters:
                switch(newState) {
                case .End:
                    throw PatternFormatterError.NotClosedMarkerParameter
                default:
                    processMarker(subPath: parserStatus.getSubPath())
                    parserStatus.charactersAccumulator.removeAll()
                }
            default:
                break
            }
            parserStatus.machineState = newState
        }

        private func processMarker(subPath: String!) {
            guard subPath != nil, !subPath.isEmpty else {
                return
            }
            let generatedClosure: FormattingClosure? = { (message) in
                guard let value = message[subPath] else {
                    throw PatternFormatterError.MissingParameter
                }
                return String(describing: value)
            }
            parsedClosuresSequence.append(generatedClosure!)
        }
    }
}
