//
//  ProcessInputLocalizable.swift
//
//  Created by Jorge Villalobos on 14/08/18.
//  Copyright © 2018 Jorge Villalobos. All rights reserved.
//

import Foundation

public protocol ProcessInputLocalizable {
    func setInputLocalizable(fileKey: String?)
    func setProperty(_ bundleId: String, _ key: String)
}

public extension ProcessInputLocalizable {
    func setInputLocalizable(fileKey: String?) {
        guard let fileKey = fileKey else {
            return
        }
        let fileKeyToFound = fileKey
            .trimmingCharacters(in: .whitespacesAndNewlines)
            .components(separatedBy: .whitespaces)
            .joined()

        let parts = fileKeyToFound.components(separatedBy: ",")
        guard let fileName = parts.first, let keyName = parts.last else {
            return
        }
        self.setProperty(fileName, keyName)
    }
}
