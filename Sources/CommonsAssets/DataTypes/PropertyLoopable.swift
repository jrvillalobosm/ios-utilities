//
//  PropertyLoopable.swift
//
//  Created by Jorge Villalobos on 14/08/18.
//  Copyright © 2018 Jorge Villalobos. All rights reserved.
//

import Foundation

public protocol PropertyLoopable {
    func allProperties() throws -> [String: Any]
}

public extension PropertyLoopable {
    func allProperties() -> [String: Any] {
        var result: [String: Any] = [:]
        let mirror = Mirror(reflecting: self)
        guard let style = mirror.displayStyle, style == .struct || style == .class else {
            return result
        }
        for (labelMaybe, valueMaybe) in mirror.children {
            guard let label = labelMaybe else {
                continue
            }
            result[label] = valueMaybe
        }
        return result
    }
}
