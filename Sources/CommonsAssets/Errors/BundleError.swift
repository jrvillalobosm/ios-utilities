//
//  BundleError.swift
//
//  Created by Jorge Villalobos on 4/5/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import Foundation

public enum PlistError: IError {

    case FileNotWritten
    case FileDoesNotExist
    case BundleDoesNotExist
    case KeyDoesNotExit

    public var code: Int {
        return 0
    }

    public var domain: String {
        return "BundleManager"
    }

    public func messageError(_ args: CVarArg...) -> String {
        switch self {
        case .BundleDoesNotExist:
            return "Can't find message for this key [%@.%@].".formatter(args: args)
        case .KeyDoesNotExit:
            return "Can't find message for this key [%@].".formatter(args: args)
        case .FileDoesNotExist:
            return "File [%@] does not exist.".formatter(args: args)
        case .FileNotWritten:
            return "File [%@] not Written.".formatter(args: args)
        }
    }

    public func messageError(_ args: [CVarArg] = []) -> String {
        switch self {
        case .BundleDoesNotExist:
            return "Can't find message for this key [%@.%@].".formatter(args: args)
        case .KeyDoesNotExit:
            return "Can't find message for this key [%@].".formatter(args: args)
        case .FileDoesNotExist:
            return "File [%@] does not exist.".formatter(args: args)
        case .FileNotWritten:
            return "File [%@] not Written.".formatter(args: args)
        }
    }

    public func getMessageError(_ bundleId: String, _ key: String) -> String! {
        switch self {
        case .BundleDoesNotExist:
            return messageError(bundleId, key)
        case .KeyDoesNotExit:
            return messageError(key)
        default:
            return messageError(bundleId)
        }
    }
}
