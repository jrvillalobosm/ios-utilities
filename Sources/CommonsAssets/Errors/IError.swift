//
//  IError.swift
//
//  Created by Jorge Villalobos on 4/5/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import Foundation

public protocol IError: Error {
    var bundleId: String { get }
    var key: String { get }
    var code: Int { get }
    var domain: String { get }

    func messageError(_ args: CVarArg...) -> String
    func messageError(_ args: [CVarArg]) -> String
}

public extension IError {
    var bundleId: String {
        return DefaultConstants.error.description
    }

    var code: Int {
        return 0
    }

    var domain: String {
        return "Error"
    }

    var key: String {
        return "\(domain)-\(code)"
    }

    func messageError(_ args: CVarArg...) -> String {
        return self.messageError(args)
    }

    func messageError(_ args: [CVarArg] = []) -> String {
        let message: String! = BundleFacade.shared.displayMessage(bundleId: self.bundleId, key: self.key, args: args)
        return message
    }
}
