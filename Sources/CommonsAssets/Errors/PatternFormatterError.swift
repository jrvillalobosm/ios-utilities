//
//  PatternFormatter.swift
//
//  Created by Jorge Villalobos on 11/12/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import Foundation

public enum PatternFormatterError: IError {
    case InvalidFormatSyntax
    case NotClosedMarkerParameter
    case MissingParameter

    public var code: Int {
        switch self {
        case .InvalidFormatSyntax:
            return 10001
        case .NotClosedMarkerParameter:
            return 10002
        case .MissingParameter:
            return 10003
        }
    }

    public var domain: String {
        return "PatternFormatter"
    }
}
