//
//  ModelType.swift
//
//  Created by Jorge Villalobos on 4/26/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import Foundation
import ObjectMapper

public typealias Json = [String: Any]

public protocol IModel: Mappable {
    func toModel() -> Json
}
