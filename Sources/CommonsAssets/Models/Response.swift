//
//  Response.swift
//
//  Created by Jorge Villalobos on 4/19/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import Foundation
import RxSwift

public enum Response<T> {

    case success(T!)
    case failure(IError)
    case multiFailure([IError])

    public var isSuccess: Bool {
        switch self {
        case .success:
            return true
        case .failure:
            return false
        case .multiFailure:
            return false
        }
    }

    public var isFailure: Bool {
        return !isSuccess
    }

    public var value: T! {
        switch self {
        case .success(let value):
            return value
        case .failure:
            return nil
        case .multiFailure:
            return nil
        }
    }

    public var error: IError? {
        switch self {
        case .success:
            return nil
        case .failure(let error):
            return error
        case .multiFailure(let errors):
            return errors.first
        }
    }

    public var errors: [IError]? {
        switch self {
        case .success:
            return nil
        case .failure(let error):
            return [error]
        case .multiFailure(let errors):
            return errors
        }
    }
}

public extension Response {

    var valueObservable: Observable<T?> {
        return Observable.create { observer in
            observer.onNext(self.value)
            observer.onCompleted()
            return Disposables.create()
        }
    }

    var errorObservable: Observable<T> {
        return Observable.create { observer in
            observer.onError(self.error!)
            return Disposables.create()
        }
    }
}

extension Response: CustomStringConvertible {

    public var description: String {
        switch self {
        case .success:
            return "SUCCESS"
        case .failure:
            return "FAILURE"
        case .multiFailure:
            return "MULTI-FAILURE"
        }
    }
}
