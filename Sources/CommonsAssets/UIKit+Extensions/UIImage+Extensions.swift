//
//  UIImage+Extensions.swift
//
//  Created by Jorge Villalobos on 15/08/18.
//  Copyright © 2018 Jorge Villalobos. All rights reserved.
//

import Foundation
import UIKit

public extension UIImage {
    func addImagePadding(top: CGFloat, right: CGFloat, bottom: CGFloat, left: CGFloat) -> UIImage? {
        let width: CGFloat = self.size.width + right + left
        let height: CGFloat = self.size.height + top + bottom
        UIGraphicsBeginImageContextWithOptions(CGSize(width: width, height: height), false, 0)
        let origin: CGPoint = CGPoint(x: left, y: top)
        self.draw(at: origin)
        let imageWithPadding = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return imageWithPadding
    }

    func resizeImage(with factor: CGFloat) -> UIImage {
        let ratio = max(factor, factor)
        let newSize = CGSize(width: size.width * ratio, height: size.height * ratio)
        return resizeImage(newSize: newSize, opaque: true, scale: 0)
    }

    func resizeImage(targetSize: CGSize) -> Data {
        let size = self.size
        let widthRatio = targetSize.width / self.size.width
        let heightRatio = targetSize.height / self.size.height
        var newSize: CGSize
        if widthRatio > heightRatio {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
        }
        let newImage = resizeImage(newSize: newSize, opaque: false, scale: 1.0)
        let imageData: Data = newImage.pngData()!
        return imageData
    }

    private func resizeImage(newSize: CGSize, opaque: Bool, scale: CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(newSize, opaque, scale)
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}
