//
//  UILabel+Extensions.swift
//
//  Created by Jorge Villalobos on 09/11/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import Foundation
import UIKit

public extension UILabel {
    func setLineHeight(lineHeight: CGFloat) {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 1.0
        paragraphStyle.lineHeightMultiple = lineHeight
        paragraphStyle.alignment = self.textAlignment

        let attrString = NSMutableAttributedString(string: self.text!)
        attrString.addAttribute(NSAttributedString.Key.font, value: self.font, range: NSMakeRange(0, attrString.length))
        attrString.addAttribute(NSAttributedString.Key.paragraphStyle,
            value: paragraphStyle,
            range: NSMakeRange(0, attrString.length))
        self.attributedText = attrString
    }

    func textToHtml(text: String, lineHeight: CGFloat, withParagraphStyle: Bool = true) {
        let data = text.data(using: String.Encoding.utf8, allowLossyConversion: true)
        let attrString: NSMutableAttributedString
        if let d = data {
            do {
                attrString = try NSMutableAttributedString(data: d,
                    options: [NSAttributedString.DocumentReadingOptionKey
                            .documentType: NSAttributedString.DocumentType.html,
                        NSAttributedString.DocumentReadingOptionKey
                            .characterEncoding: String.Encoding.utf8.rawValue],
                    documentAttributes: nil)
            } catch {
                attrString = NSMutableAttributedString(string: text)
            }
        } else {
            attrString = NSMutableAttributedString(string: text)
        }
        if withParagraphStyle {
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = 1.0
            paragraphStyle.lineHeightMultiple = lineHeight
            paragraphStyle.alignment = self.textAlignment

            attrString.addAttribute(NSAttributedString.Key.font,
                value: self.font,
                range: NSMakeRange(0, attrString.length))
            attrString.addAttribute(NSAttributedString.Key.paragraphStyle,
                value: paragraphStyle,
                range: NSMakeRange(0, attrString.length))
        }
        self.attributedText = attrString
    }

    func heightForFrame() -> CGFloat {
        let rect = CGRect(x: 0, y: 0, width: self.frame.width, height: CGFloat.greatestFiniteMagnitude)
        let label: UILabel = UILabel(frame: rect)
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = self.font
        label.text = (self.text) ?? ""

        label.sizeToFit()
        return label.frame.height
    }

    func colorString(text: String, coloredText: String, color: UIColor? = #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1)) {
        let attributedString = NSMutableAttributedString(string: text)
        let range = (text.uppercased() as NSString).range(of: coloredText.uppercased())
        attributedString.setAttributes([NSAttributedString.Key.foregroundColor: color!],
            range: range)
        self.attributedText = attributedString
    }
}
