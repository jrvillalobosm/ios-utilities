//
//  UINavigationController+Extensions.swift
//
//  Created by Jorge Villalobos on 24/04/18.
//  Copyright © 2018 Jorge Villalobos. All rights reserved.
//

import Foundation
import UIKit

public extension UINavigationController {
    func setupTabBarItem(title: String?,
        imageName: String,
        selectedImageName: String,
        tintColor: UIColor = .gray
    ) {
        let image = UIImage(named: imageName)?.withRenderingMode(.alwaysOriginal)
        let selectedImage = UIImage(named: selectedImageName)?.withRenderingMode(.alwaysOriginal)
        tabBarItem = UITabBarItem(title: title, image: image, selectedImage: selectedImage)
        tabBarController?.tabBar.tintColor = tintColor
    }

    func pushViewController(_ viewController: UIViewController,
        animated: Bool,
        completion: @escaping () -> Void
    ) {
        pushViewController(viewController, animated: animated)

        if animated, let coordinator = transitionCoordinator {
            coordinator.animate(alongsideTransition: nil) { _ in
                completion()
            }
        } else {
            completion()
        }
    }

    func popToViewController(_ viewController: UIViewController,
        animated: Bool,
        completion: @escaping () -> Void
    ) {
        popToViewController(viewController, animated: animated)
        if animated, let coordinator = transitionCoordinator {
            coordinator.animate(alongsideTransition: nil) { _ in
                completion()
            }
        } else {
            completion()
        }
    }

    func popViewController(animated: Bool, completion: @escaping () -> Void) {
        popViewController(animated: animated)

        if animated, let coordinator = transitionCoordinator {
            coordinator.animate(alongsideTransition: nil) { _ in
                completion()
            }
        } else {
            completion()
        }
    }
}
