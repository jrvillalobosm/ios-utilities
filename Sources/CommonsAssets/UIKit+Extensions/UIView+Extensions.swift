//
//  UIView+Extensions.swift
//
//  Created by Jorge Villalobos on 31/01/18.
//  Copyright © 2018 Jorge Villalobos. All rights reserved.
//

import Foundation
import UIKit

public extension UIView {
    var firstResponder: UIView? {
        guard !isFirstResponder else { return self }
        for subview in subviews {
            if let firstResponder = subview.firstResponder {
                return firstResponder
            }
        }
        return nil
    }

    enum TypeBorder {
        case top
        case bottom
        case left
        case right
    }
    //Esto solo de debe usar cuando el elemento tenga un tamaño fijo!
    func addLayerBorder(typeBorder: TypeBorder, color: UIColor, thickness: CGFloat = 1.0) {
        let border = CALayer()
        switch typeBorder {
        case .top:
            border.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: thickness)
            break
        case .bottom:
            border.frame = CGRect(x: 0, y: self.frame.height - thickness, width: self.frame.width, height: thickness)
            break
        case .left:
            border.frame = CGRect(x: 0, y: 0, width: thickness, height: self.frame.height)
            break
        case .right:
            border.frame = CGRect(x: self.frame.width - thickness, y: 0, width: thickness, height: self.frame.height)
            break
        }

        border.backgroundColor = color.cgColor;
        self.layer.addSublayer(border)
    }

    func addViewBorder(typeBorder: TypeBorder, color: UIColor, thickness: CGFloat = 1.0) {
        let border = UIView()
        switch typeBorder {
        case .top:
            border.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: thickness)
            break
        case .bottom:
            border.frame = CGRect(x: 0, y: self.frame.height - thickness, width: self.frame.width, height: thickness)
            break
        case .left:
            border.frame = CGRect(x: 0, y: 0, width: thickness, height: self.frame.height)
            break
        case .right:
            border.frame = CGRect(x: self.frame.width - thickness, y: 0, width: thickness, height: self.frame.height)
            break
        }

        border.backgroundColor = color
        border.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin]
        self.addSubview(border)
    }

    class func removeClearButton(svs: [UIView]) {
        for sv in svs {
            if let tv = sv as? UITextField, sv.conforms(to: UITextInputTraits.self) {
                tv.clearButtonMode = .never
                return
            } else {
                UIView.removeClearButton(svs: sv.subviews)
            }
        }
    }
}
