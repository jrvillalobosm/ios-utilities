//
//  UIViewController+Extensions.swift
//
//  Created by Jorge Villalobos on 16/08/18.
//  Copyright © 2018 Jorge Villalobos. All rights reserved.
//

import Foundation
import UIKit

public extension UIViewController {
    func backToViewController(byDeep nb: Int, animated: Bool = true) {
        guard nb > 0, let navigationController = self.navigationController else {
            return
        }
        let viewControllers: [UIViewController] = navigationController.viewControllers
        guard viewControllers.count < nb else {
            navigationController.popToViewController(viewControllers[viewControllers.count - nb], animated: animated)
            return
        }
    }

    func backToViewController<T: UIViewController>(byType toControllerType: T.Type, animated: Bool) {
        guard let navigationController = self.navigationController else {
            return
        }
        var viewControllers: [UIViewController] = navigationController.viewControllers
        viewControllers = viewControllers.reversed()
        for currentViewController in viewControllers {
            if currentViewController .isKind(of: toControllerType) {
                navigationController.popToViewController(currentViewController, animated: animated)
                break
            }
        }
    }

    func onKeyboardWillHide() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func hideKeyboard() {
        if let firstResponder = view.window?.firstResponder {
            firstResponder.resignFirstResponder()
        }
    }
}
