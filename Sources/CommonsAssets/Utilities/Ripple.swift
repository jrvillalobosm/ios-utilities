//
//  Ripple.swift
//
//  Created by Jorge Villalobos on 25/07/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import Foundation
import UIKit

open class Ripple: NSObject, CAAnimationDelegate {
    public var center: CGPoint
    public var view: UIView
    public var duration: TimeInterval
    public var size: CGSize
    public var multiplier: CGFloat
    public var borderColor: UIColor
    public var backgroundColor: UIColor
    public var border: CGFloat
    public var ripple: UIView = UIView()
    public var isCircle: Bool

    public init(point: CGPoint?,
        view: UIView,
        duration: TimeInterval,
        size: CGFloat,
        multiplier: CGFloat = 2.0,
        borderColor: UIColor,
        backgroundColor: UIColor = UIColor.clear,
        border: CGFloat = 2.25,
        isCircle: Bool = true
    ) {
        if(point == nil) {
            let frame = view.convert(view.frame, from: view.superview)
            self.center = CGPoint(x: frame.midX, y: frame.midY)
        } else {
            self.center = point!
        }
        self.view = view
        self.duration = duration
        self.size = CGSize(width: size, height: size) //size
        self.multiplier = multiplier
        self.borderColor = borderColor
        self.backgroundColor = backgroundColor
        self.border = border
        self.isCircle = isCircle
        self.view.clipsToBounds = true //AGREGADO
    }

    public func put() {
        if let subview = view.subviews.first {
            view.insertSubview(ripple, aboveSubview: subview)
        } else {
            view.insertSubview(ripple, at: 0)
        }
        ripple.frame.origin = CGPoint(x: center.x - size.width / 2,
            y: center.y - size.height / 2)
        ripple.frame.size = size
        ripple.layer.borderColor = borderColor.cgColor
        ripple.layer.backgroundColor = backgroundColor.cgColor
        ripple.layer.borderWidth = border
        if(isCircle) {
            ripple.layer.cornerRadius = ripple.bounds.width / 2
        }

    }

    public func clear() {
        ripple.removeFromSuperview()
    }

    public func activate() {
        if let subview = view.subviews.first {
            view.insertSubview(ripple, aboveSubview: subview)
        } else {
            view.insertSubview(ripple, at: 0)
        }
        ripple.frame.origin = CGPoint(x: center.x - size.width / 2,
            y: center.y - size.height / 2)
        ripple.frame.size = size
        ripple.layer.borderColor = borderColor.cgColor
        ripple.layer.backgroundColor = backgroundColor.cgColor
        ripple.layer.borderWidth = border
        if(isCircle) {
            ripple.layer.cornerRadius = ripple.bounds.width / 2
        }
        let animation = CABasicAnimation(keyPath: "cornerRadius")
        animation.fromValue = ripple.layer.cornerRadius
        animation.toValue = size.width * multiplier / 2
        let boundsAnimation = CABasicAnimation(keyPath: "bounds.size")
        boundsAnimation.fromValue = NSValue(cgSize: ripple.layer.bounds.size)
        let newSize = CGSize(width: size.width * multiplier, height: size.height * multiplier)
        boundsAnimation.toValue = NSValue(cgSize: newSize)

        let opacityAnimation = CAKeyframeAnimation(keyPath: "opacity")
        opacityAnimation.values = [0, 1, 1, 1, 0]

        let animationGroup = CAAnimationGroup()
        var animations = [animation, boundsAnimation, opacityAnimation]
        if !(isCircle) {
            animations.remove(at: 0)
        }
        animationGroup.animations = animations
        animationGroup.duration = duration
        animationGroup.delegate = self
        animationGroup.timingFunction = CAMediaTimingFunction(controlPoints: 0.22, 0.54, 0.2, 0.47)
        animationGroup.isRemovedOnCompletion = false
        animationGroup.fillMode = CAMediaTimingFillMode.forwards

        //ripples.append(ripple)
        ripple.layer.add(animationGroup, forKey: "ripple")
    }

    public func timerDidFire() {
        activate()
    }

    open func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        ripple.removeFromSuperview()
    }
}
