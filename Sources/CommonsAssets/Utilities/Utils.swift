//
//  Utils.swift
//
//  Created by Jorge Villalobos on 16/02/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import Foundation
import UIKit

public class Utils {
    public class func getDataFromURL(fileURL: String) throws -> NSData? {
        var data: NSData
        do {
            try data = Data(contentsOf: URL(string: fileURL)!) as NSData
        } catch {
            return nil
        }
        return data
    }
}
