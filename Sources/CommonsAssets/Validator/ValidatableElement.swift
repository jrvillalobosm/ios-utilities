//
//  ValidatableElement.swift
//
//  Created by Jorge Villalobos on 07/02/19.
//  Copyright © 2019 Jorge Villalobos. All rights reserved.
//
/*
import Foundation
import ObjectiveC
import UIKit
import Validator

public enum ValidatorError: IError {
    case unknown(Error)
    
    public var code: Int { return 0 }
    
    public var domain: String { return "Validator" }
    
    public func getMessage(_ args: [CVarArg]) -> String {
        switch self {
        case let .unknown(error):
            return error.localizedDescription
        }
    }
}

public protocol ValidatableElement: AnyObject {
    
    var successHandler: ((ValidationResult) -> Void) { get set }
    var failureHandler: (([IError]?) -> Void) { get set }
    var isFirstTime: Bool { get set }
}

struct AssociatedKeys {
    static var onShouldReturn: UInt8 = 0
    static var successHandler: UInt8 = 0
    static var failureHandler: UInt8 = 0
    static var isFirstTime: UInt8 = 0
}

extension ValidatableElement {
    public var onShouldReturn: ((UITextField) -> Bool) {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.onShouldReturn) as! (UITextField) -> Bool
        }
        set(newValue) {
            objc_setAssociatedObject(self,
                                     &AssociatedKeys.onShouldReturn,
                                     newValue as AnyObject,
                                     objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    public var successHandler: ((ValidationResult) -> Void) {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.successHandler) as! (ValidationResult) -> Void
        }
        set(newValue) {
            objc_setAssociatedObject(self,
                                     &AssociatedKeys.successHandler,
                                     newValue as AnyObject,
                                     objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    public var failureHandler: (([IError]?) -> Void) {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.failureHandler) as! ([IError]?) -> Void
        }
        set(newValue) {
            objc_setAssociatedObject(self,
                                     &AssociatedKeys.failureHandler,
                                     newValue as AnyObject,
                                     objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    public var isFirstTime: Bool {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.isFirstTime) as! Bool
        }
        set(newValue) {
            objc_setAssociatedObject(self,
                                     &AssociatedKeys.isFirstTime,
                                     newValue as AnyObject,
                                     objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
}

extension UITextField: ValidatableElement, UITextFieldDelegate {
    public func validateOnInputChange(enabled: Bool) {
        self.delegate = self
    }
    
    public func validateOnEditingEnd(enabled: Bool) {
        self.delegate = self
    }
    
    public func textFieldShouldReturn(_: UITextField) -> Bool {
        return onShouldReturn(self)
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn _: NSRange, replacementString _: String) -> Bool {
        let deadline = DispatchTime.now() + .milliseconds(100)
        DispatchQueue.main.asyncAfter(deadline: deadline, execute: { [weak self] in
            guard let weakSelf = self else { return }
            weakSelf.textFieldValidate(true)
        })
        return true
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        self.textFieldValidate()
        self.isFirstTime = false
    }
    
    fileprivate func textFieldValidate(_ isInitialValue: Bool = false) {
        let hasError: ValidationResult = self.validate()
        switch hasError {
        case .valid:
            self.changeBorderColor(true)
            self.successHandler(hasError)
        case .invalid(let failureErrors):
            var errors: [IError]?
            if !isInitialValue || isInitialValue && !self.isFirstTime {
                errors = failureErrors.map() { error -> IError in
                    if let error = error as? IError {
                        return error
                    } else {
                        return ValidatorError.unknown(error)
                    }
                }
                self.changeBorderColor(false)
            }
            self.failureHandler(errors)
        }
    }
    
    func changeBorderColor(_ isSuccess: Bool) {
        if isSuccess {
            self.layer.borderColor = UIColor(red: 136 / 255, green: 136 / 255, blue: 136 / 255, alpha: 1.0).cgColor
        } else {
            self.layer.borderColor = UIColor.red.cgColor
        }
        self.layer.borderWidth = 0.8
        self.layer.cornerRadius = 5
    }
}
// */
