//
//  ValidationPattern.swift
//
//  Created by Jorge Villalobos on 4/26/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import Foundation

public protocol ValidationPattern {

    var pattern: String { get }
}

public enum EmailValidationPattern: ValidationPattern {

    case simple
    case standard

    public var pattern: String {
        switch self {
        case .simple:
            return "^.+@.+\\..+$"
        case .standard:
            return "^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-+]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z‌​]{2,})$"
        }
    }
}
