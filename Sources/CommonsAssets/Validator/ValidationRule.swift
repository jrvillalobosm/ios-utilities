//
//  ValidationRule.swift
//
//  Created by Jorge Villalobos on 4/26/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import Foundation

public protocol ValidationRule {

    associatedtype InputType

    func validate(input: InputType?) -> Bool

    var error: IError { get }
}

internal struct AnyValidationRule<InputType>: ValidationRule {

    private let baseValidateInput: (InputType?) -> Bool

    let error: IError

    init<R: ValidationRule>(base: R) where R.InputType == InputType {
        baseValidateInput = base.validate
        error = base.error
    }

    func validate(input: InputType?) -> Bool {
        return baseValidateInput(input)
    }
}

public struct ValidationRuleLength: ValidationRule {

    public enum LengthType {
        case characters
        case utf8
        case utf16
        case unicodeScalars
    }

    public typealias InputType = String

    public var error: IError

    public let min: Int

    public let max: Int

    public let lengthType: LengthType

    public init(min: Int = 0, max: Int = Int.max, lengthType: LengthType = .characters, error: IError) {
        self.min = min
        self.max = max
        self.lengthType = lengthType
        self.error = error
    }

    public func validate(input: String?) -> Bool {
        guard let input = input else { return false }

        let length: Int
        switch lengthType {
        case .characters: length = input.count
        case .utf8: length = input.utf8.count
        case .utf16: length = input.utf16.count
        case .unicodeScalars: length = input.unicodeScalars.count
        }

        return length >= min && length <= max
    }
}

public struct ValidationRulePattern: ValidationRule {

    public typealias InputType = String
    public let error: IError

    public let pattern: String

    public init(pattern: String, error: IError) {
        self.pattern = pattern
        self.error = error
    }

    public init(pattern: ValidationPattern, error: IError) {
        self.init(pattern: pattern.pattern, error: error)
    }

    public func validate(input: String?) -> Bool {
        return NSPredicate(format: "SELF MATCHES %@", pattern).evaluate(with: input)
    }
}

public struct ValidationRuleCondition<T>: ValidationRule {

    public typealias InputType = T

    public let error: IError

    public let condition: (T?) -> Bool

    public init(error: IError, condition: @escaping ((T?) -> Bool)) {
        self.condition = condition
        self.error = error
    }

    public func validate(input: T?) -> Bool {
        return condition(input)
    }
}

public struct ValidationRuleEquality<T: Equatable>: ValidationRule {

    public typealias InputType = T

    public let error: IError

    let target: T?

    let dynamicTarget: (() -> T)?

    public init(target: T, error: IError) {
        self.target = target
        self.error = error
        self.dynamicTarget = nil
    }

    public init(dynamicTarget: @escaping (() -> T), error: IError) {
        self.dynamicTarget = dynamicTarget
        self.error = error
        self.target = nil
    }

    public func validate(input: T?) -> Bool {
        if let dynamicTarget = dynamicTarget {
            return input == dynamicTarget()
        }

        guard let target = target else { return false }
        return input == target
    }
}
