//
//  Validator.swift
//
//  Created by Jorge Villalobos on 4/26/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import Foundation

public struct Validator {

    public static func validate<R: ValidationRule>(input: R.InputType?, rule: R) -> Response<Void> {

        var ruleSet = ValidationRuleSet<R.InputType>()
        ruleSet.add(rule: rule)
        return Validator.validate(input: input, rules: ruleSet)
    }

    public static func validate<T>(input: T?, rules: ValidationRuleSet<T>) -> Response<Void> {
        let errors = rules.rules.filter { !$0.validate(input: input) }.map { $0.error }
        return errors.isEmpty ? Response.success(nil) : Response.multiFailure(errors)
    }
}

public struct ValidationRuleSet<InputType> {

    public init() {

    }

    public init<R: ValidationRule>(rules: [R]) where R.InputType == InputType {
        self.rules = rules.map(AnyValidationRule.init)
    }

    internal var rules = [AnyValidationRule<InputType>]()


    public mutating func add<R: ValidationRule>(rule: R) where R.InputType == InputType {
        let anyRule = AnyValidationRule(base: rule)
        rules.append(anyRule)
    }
}

public protocol Validatable {

    func validate<R: ValidationRule>(rule: R) -> Response<Void> where R.InputType == Self

    func validate(rules: ValidationRuleSet<Self>) -> Response<Void>
}

public extension Validatable {

    func validate<R: ValidationRule>(rule: R) -> Response<Void> where R.InputType == Self {
        return Validator.validate(input: self, rule: rule)
    }

    func validate(rules: ValidationRuleSet<Self>) -> Response<Void> {
        return Validator.validate(input: self, rules: rules)
    }
}

extension String: Validatable { }
extension Int: Validatable { }
extension Double: Validatable { }
extension Float: Validatable { }
extension Array: Validatable { }
extension Date: Validatable { }
