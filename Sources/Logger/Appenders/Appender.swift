//
//  Appender.swift
//
//  Created by Jorge Villalobos on 4/6/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import Foundation

@objc open class Appender: NSObject {

    let identifier: String
    public var thresholdLevel = Level.debug
    internal var filters = [FilterType]()
    public var formatter: Layout?

    open var onAppend: (String, Level, LogInfoDictionary) -> () = { _, _, _ in }

    public required init(_ identifier: String) {
        self.identifier = identifier
    }

    internal func update(withDictionary dictionary: Dictionary<String, Any>,
        availableFormatters: Array<Layout>
    ) throws {
        if let safeThresholdString = (dictionary[DictionaryKey.ThresholdLevel] as? String) {
            if let safeThreshold = Level(safeThresholdString) {
                thresholdLevel = safeThreshold
            } else {
                throw LoggerError.InvalidLevel
            }
        }

        if let safeFormatterId = (dictionary[DictionaryKey.FormatterId] as? String) {
            if let formatter = availableFormatters.find(filter: { $0.identifier == safeFormatterId }) {
                self.formatter = formatter
            } else {
                let messageError: String = "No such formatter '\(safeFormatterId)' for appender \(self.identifier)"
                throw LoggerError.NoSuchFormatterForAppender(messageError)
            }
        }
    }

    public func append(_ log: String, level: Level, info: LogInfoDictionary) { } // To be overriden by subclasses

    final public func log(_ log: String, level: Level, info: LogInfoDictionary) {
        if(level >= self.thresholdLevel) {
            let logMessage: String

            if let formatter = self.formatter {
                logMessage = formatter.format(message: log, info: info)
            } else {
                logMessage = log
            }

            self.append(logMessage, level: level, info: info)
        }
    }
}

public extension Appender {

    func addFilter(_ filter: FilterType) {
        filters.append(filter)
    }

    func removeFilter(_ filter: FilterType) {
        let index = filters.firstIndex {
            return ObjectIdentifier($0) == ObjectIdentifier(filter)
        }

        guard let filterIndex = index else {
            return
        }

        filters.remove(at: filterIndex)
    }

    func hasMessageFilters() -> Bool {
        return !getFiltersTargeting(TargetType.Message(.Equals([], true)), fromFilters: self.filters).isEmpty
    }

    func shouldLevelBeLogged(_ level: Level, path: String, function: String, message: String? = nil) -> Bool {

        if filters.isEmpty {
            if level >= thresholdLevel {
                return true
            } else {
                return false
            }
        }

        let (matchedExclude, allExclude) =
            passedExcludedFilters(level,
                path: path,
                function: function,
                message: message)
        if allExclude > 0 && matchedExclude != allExclude {
            return false
        }

        if level >= thresholdLevel {
            return true
        }

        let (matchedRequired, allRequired) =
            passedRequiredFilters(level,
                path: path,
                function: function,
                message: message)
        let (matchedNonRequired, allNonRequired) =
            passedNonRequiredFilters(level,
                path: path,
                function: function,
                message: message)
        if allRequired > 0 {
            if matchedRequired == allRequired {
                return true
            }
        } else {
            if allNonRequired > 0 {
                if matchedNonRequired > 0 {
                    return true
                }
            } else if allExclude == 0 {
                return true
            }
        }
        return false
    }

    private func getFiltersTargeting(_ target: TargetType, fromFilters: [FilterType]) -> [FilterType] {
        return fromFilters.filter { filter in
            return filter.getTarget() == target
        }
    }

    private func passedRequiredFilters(_ level: Level,
        path: String,
        function: String,
        message: String?
    ) -> (Int, Int) {
        let requiredFilters = self.filters.filter { filter in
            return filter.isRequired() && !filter.isExcluded()
        }

        let matchingFilters = applyFilters(requiredFilters,
            level: level,
            path: path,
            function: function,
            message: message)
        return (matchingFilters, requiredFilters.count)
    }

    private func passedNonRequiredFilters(_ level: Level,
        path: String,
        function: String,
        message: String?
    ) -> (Int, Int) {
        let nonRequiredFilters = self.filters.filter { filter in
            return !filter.isRequired() && !filter.isExcluded()
        }

        let matchingFilters = applyFilters(nonRequiredFilters,
            level: level,
            path: path,
            function: function,
            message: message)
        return (matchingFilters, nonRequiredFilters.count)
    }

    private func passedExcludedFilters(_ level: Level,
        path: String,
        function: String,
        message: String?
    ) -> (Int, Int) {
        let excludeFilters = self.filters.filter { filter in
            return filter.isExcluded()
        }

        let matchingFilters = applyFilters(excludeFilters,
            level: level,
            path: path,
            function: function,
            message: message)
        return (matchingFilters, excludeFilters.count)
    }

    private func applyFilters(_ targetFilters: [FilterType],
        level: Level,
        path: String,
        function: String,
        message: String?
    ) -> Int {
        return targetFilters.filter { filter in
            let passes: Bool

            if !filter.reachedMinLevel(level) {
                return false
            }

            switch filter.getTarget() {
            case .Path(_):
                passes = filter.apply(path)
            case .Function(_):
                passes = filter.apply(function)
            case .Message(_):
                guard let message = message else {
                    return false
                }
                passes = filter.apply(message)
            }
            return passes
        }.count
    }
}

public func == (lhs: Appender, rhs: Appender) -> Bool {
    return ObjectIdentifier(lhs) == ObjectIdentifier(rhs)
}
