//
//  CustomAppender.swift
//
//  Created by Jorge Villalobos on 4/6/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import Foundation

public class CustomAppender: Appender {

    override public func append(_ log: String, level: Level, info: LogInfoDictionary) {
        onAppend(log, level, info)
    }
}
