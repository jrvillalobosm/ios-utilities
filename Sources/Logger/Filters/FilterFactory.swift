//
//  FilterFactory.swift
//
//  Created by Jorge Villalobos on 4/6/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import Foundation

public protocol FilterFactory {
    static func startsWith(_ prefixes: String..., caseSensitive: Bool, required: Bool, minLevel: Level) -> FilterType
    static func contains(_ strings: String..., caseSensitive: Bool, required: Bool, minLevel: Level) -> FilterType
    static func excludes(_ strings: String..., caseSensitive: Bool, required: Bool, minLevel: Level) -> FilterType
    static func endsWith(_ suffixes: String..., caseSensitive: Bool, required: Bool, minLevel: Level) -> FilterType
    static func equals(_ strings: String..., caseSensitive: Bool, required: Bool, minLevel: Level) -> FilterType
}

public class FunctionFilterFactory: FilterFactory {
    public static func startsWith(_ prefixes: String...,
        caseSensitive: Bool = false,
        required: Bool = false,
        minLevel: Level = .verbose
    ) -> FilterType {
        return CompareFilter(.Function(.StartsWith(prefixes, caseSensitive)), required: required, minLevel: minLevel)
    }

    public static func contains(_ strings: String...,
        caseSensitive: Bool = false,
        required: Bool = false,
        minLevel: Level = .verbose
    ) -> FilterType {
        return CompareFilter(.Function(.Contains(strings, caseSensitive)), required: required, minLevel: minLevel)
    }

    public static func excludes(_ strings: String...,
        caseSensitive: Bool = false,
        required: Bool = false,
        minLevel: Level = .verbose
    ) -> FilterType {
        return CompareFilter(.Function(.Excludes(strings, caseSensitive)), required: required, minLevel: minLevel)
    }

    public static func endsWith(_ suffixes: String...,
        caseSensitive: Bool = false,
        required: Bool = false,
        minLevel: Level = .verbose
    ) -> FilterType {
        return CompareFilter(.Function(.EndsWith(suffixes, caseSensitive)), required: required, minLevel: minLevel)
    }

    public static func equals(_ strings: String...,
        caseSensitive: Bool = false,
        required: Bool = false,
        minLevel: Level = .verbose
    ) -> FilterType {
        return CompareFilter(.Function(.Equals(strings, caseSensitive)), required: required, minLevel: minLevel)
    }
}

public class MessageFilterFactory: FilterFactory {
    public static func startsWith(_ prefixes: String...,
        caseSensitive: Bool = false,
        required: Bool = false,
        minLevel: Level = .verbose
    ) -> FilterType {
        return CompareFilter(.Message(.StartsWith(prefixes, caseSensitive)), required: required, minLevel: minLevel)
    }

    public static func contains(_ strings: String...,
        caseSensitive: Bool = false,
        required: Bool = false,
        minLevel: Level = .verbose
    ) -> FilterType {
        return CompareFilter(.Message(.Contains(strings, caseSensitive)), required: required, minLevel: minLevel)
    }

    public static func excludes(_ strings: String...,
        caseSensitive: Bool = false,
        required: Bool = false,
        minLevel: Level = .verbose
    ) -> FilterType {
        return CompareFilter(.Message(.Excludes(strings, caseSensitive)), required: required, minLevel: minLevel)
    }

    public static func endsWith(_ suffixes: String...,
        caseSensitive: Bool = false,
        required: Bool = false,
        minLevel: Level = .verbose
    ) -> FilterType {
        return CompareFilter(.Message(.EndsWith(suffixes, caseSensitive)), required: required, minLevel: minLevel)
    }

    public static func equals(_ strings: String...,
        caseSensitive: Bool = false,
        required: Bool = false,
        minLevel: Level = .verbose
    ) -> FilterType {
        return CompareFilter(.Message(.Equals(strings, caseSensitive)), required: required, minLevel: minLevel)
    }
}

public class PathFilterFactory: FilterFactory {
    public static func startsWith(_ prefixes: String...,
        caseSensitive: Bool = false,
        required: Bool = false,
        minLevel: Level = .verbose
    ) -> FilterType {
        return CompareFilter(.Path(.StartsWith(prefixes, caseSensitive)), required: required, minLevel: minLevel)
    }

    public static func contains(_ strings: String...,
        caseSensitive: Bool = false,
        required: Bool = false,
        minLevel: Level = .verbose
    ) -> FilterType {
        return CompareFilter(.Path(.Contains(strings, caseSensitive)), required: required, minLevel: minLevel)
    }

    public static func excludes(_ strings: String...,
        caseSensitive: Bool = false,
        required: Bool = false,
        minLevel: Level = .verbose
    ) -> FilterType {
        return CompareFilter(.Path(.Excludes(strings, caseSensitive)), required: required, minLevel: minLevel)
    }

    public static func endsWith(_ suffixes: String...,
        caseSensitive: Bool = false,
        required: Bool = false,
        minLevel: Level = .verbose
    ) -> FilterType {
        return CompareFilter(.Path(.EndsWith(suffixes, caseSensitive)), required: required, minLevel: minLevel)
    }

    public static func equals(_ strings: String...,
        caseSensitive: Bool = false,
        required: Bool = false,
        minLevel: Level = .verbose
    ) -> FilterType {
        return CompareFilter(.Path(.Equals(strings, caseSensitive)), required: required, minLevel: minLevel)
    }
}
