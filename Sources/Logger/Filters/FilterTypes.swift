//
//  FilterTypes.swift
//
//  Created by Jorge Villalobos on 4/6/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import Foundation

public enum ComparisonType {
    case StartsWith([String], Bool)
    case Contains([String], Bool)
    case Excludes([String], Bool)
    case EndsWith([String], Bool)
    case Equals([String], Bool)
}

public enum TargetType: Equatable {
    case Path(ComparisonType)
    case Function(ComparisonType)
    case Message(ComparisonType)
}

public func == (lhs: TargetType, rhs: TargetType) -> Bool {
    switch (lhs, rhs) {
    case (.Path(_), .Path(_)):
        return true
    case (.Function(_), .Function(_)):
        return true
    case (.Message(_), .Message(_)):
        return true
    default:
        return false
    }
}
