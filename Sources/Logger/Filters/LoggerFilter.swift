//
//  LoggerFilter.swift
//
//  Created by Jorge Villalobos on 4/6/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import Foundation

public protocol FilterType: class {
    func apply(_ value: Any) -> Bool
    func getTarget() -> TargetType
    func isRequired() -> Bool
    func isExcluded() -> Bool
    func reachedMinLevel(_ level: Level) -> Bool
}

public class Filters {
    public static let Path = PathFilterFactory.self
    public static let Function = FunctionFilterFactory.self
    public static let Message = MessageFilterFactory.self
}

public class LoggerFilter {
    private let targetType: TargetType
    private let required: Bool
    private let minLevel: Level

    public init(_ target: TargetType, required: Bool, minLevel: Level) {
        self.targetType = target
        self.required = required
        self.minLevel = minLevel
    }

    public func getTarget() -> TargetType {
        return self.targetType
    }

    public func isRequired() -> Bool {
        return self.required
    }

    public func isExcluded() -> Bool {
        return false
    }

    public func reachedMinLevel(_ level: Level) -> Bool {
        return level >= minLevel
    }
}

public class CompareFilter: LoggerFilter, FilterType {
    private var filterComparisonType: ComparisonType?

    override public init(_ target: TargetType, required: Bool, minLevel: Level) {
        super.init(target, required: required, minLevel: minLevel)
        let comparisonType: ComparisonType?
        switch self.getTarget() {
        case let .Function(comparison):
            comparisonType = comparison
        case let .Path(comparison):
            comparisonType = comparison
        case let .Message(comparison):
            comparisonType = comparison
        }
        self.filterComparisonType = comparisonType
    }

    public func apply(_ value: Any) -> Bool {
        guard let value = value as? String else {
            return false
        }

        guard let filterComparisonType = self.filterComparisonType else {
            return false
        }

        let matches: Bool
        switch filterComparisonType {
        case let .Contains(strings, caseSensitive):
            matches = !strings.filter { string in
                return caseSensitive ? value.contains(string) : value.lowercased().contains(string.lowercased())
            }.isEmpty
        case let .Excludes(strings, caseSensitive):
            matches = !strings.filter { string in
                return caseSensitive ? !value.contains(string) : !value.lowercased().contains(string.lowercased())
            }.isEmpty
        case let .StartsWith(strings, caseSensitive):
            matches = !strings.filter { string in
                return caseSensitive ? value.hasPrefix(string) : value.lowercased().hasPrefix(string.lowercased())
            }.isEmpty
        case let .EndsWith(strings, caseSensitive):
            matches = !strings.filter { string in
                return caseSensitive ? value.hasSuffix(string) : value.lowercased().hasSuffix(string.lowercased())
            }.isEmpty
        case let .Equals(strings, caseSensitive):
            matches = !strings.filter { string in
                return caseSensitive ? value == string: value.lowercased() == string.lowercased()
            }.isEmpty
        }
        return matches
    }

    override public func isExcluded() -> Bool {
        guard let filterComparisonType = self.filterComparisonType else { return false }
        switch filterComparisonType {
        case .Excludes(_, _):
            return true
        default:
            return false
        }
    }
}
