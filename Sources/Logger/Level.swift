//
//  Level.swift
//
//  Created by Jorge Villalobos on 4/6/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import Foundation

@objc public enum Level: Int, CustomStringConvertible {
    case verbose = 0
    case debug = 1
    case info = 2
    case warning = 3
    case error = 4
    case fatal = 5
    case off = 6

    public init?(_ stringValue: String) {
        switch(stringValue.lowercased()) {
        case Level.verbose.description.lowercased():
            self = .verbose
        case Level.debug.description.lowercased():
            self = .debug
        case Level.info.description.lowercased():
            self = .info
        case Level.warning.description.lowercased():
            self = .warning
        case Level.error.description.lowercased():
            self = .error
        case Level.fatal.description.lowercased():
            self = .fatal
        case Level.off.description.lowercased():
            self = .off
        default:
            return nil
        }
    }

    public var description: String {
        get {
            switch(self) {
            case .verbose:
                return "VERBOSE"
            case .debug:
                return "DEBUG"
            case .info:
                return "INFO"
            case .warning:
                return "WARN"
            case .error:
                return "ERROR"
            case .fatal:
                return "FATAL"
            case .off:
                return "OFF"
            }
        }
    }
}

public func >= (a: Level, b: Level) -> Bool {
    return a.rawValue >= b.rawValue
}
