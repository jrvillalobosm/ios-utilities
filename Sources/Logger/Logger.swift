//
//  Logger.swift
//
//  Created by Jorge Villalobos on 4/6/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import CommonsAssets
import Foundation

@objc public final class Logger: NSObject {
    public class func getLogger(_ identifier: String) -> Logger {
        return LoggerFactory.shared.getLogger(identifier)
    }

    private static let loggingQueue: DispatchQueue = {
        let createdQueue: DispatchQueue

        if #available(OSX 10.10, *) {
            createdQueue = DispatchQueue(label: "logger-queue-", qos: .background, attributes: [])
        } else {
            let backgroundQueue = DispatchQueue.global(priority: .background)
            createdQueue = DispatchQueue(label: "logger-queue-", attributes: [], target: backgroundQueue)
        }
        return createdQueue
    }()

    public let identifier: String

    internal var parent: Logger?

    private var thresholdLevelStorage: Level
    private var appendersStorage: [Appender]
    private var asynchronousStorage = false

    public var asynchronous: Bool {
        get {
            if let parent = self.parent {
                return parent.asynchronous
            } else {
                return self.asynchronousStorage
            }
        }
        set {
            self.breakDependencyWithParent()
            self.asynchronousStorage = newValue
        }
    }

    public var thresholdLevel: Level {
        get {
            if let parent = self.parent {
                return parent.thresholdLevel
            } else {
                return self.thresholdLevelStorage
            }
        }
        set {
            self.breakDependencyWithParent()
            self.thresholdLevelStorage = newValue
        }
    }

    public var appenders: [Appender] {
        get {
            if let parent = self.parent {
                return parent.appenders
            } else {
                return self.appendersStorage
            }
        }
        set {
            self.breakDependencyWithParent()
            self.appendersStorage = newValue
        }
    }


    public init(identifier: String, level: Level = Level.debug, appenders: [Appender] = []) {
        self.identifier = identifier
        self.thresholdLevelStorage = level
        self.appendersStorage = appenders
    }

    convenience override init() {
        self.init(identifier: "", appenders: Logger.createDefaultAppenders())
    }

    convenience init(parentLogger: Logger, identifier: String) {
        self.init(identifier: identifier,
            level: parentLogger.thresholdLevel,
            appenders: [Appender]() + parentLogger.appenders)
        self.parent = parentLogger
    }

    internal func update(withDictionary dictionary: Dictionary<String, Any>,
        availableAppenders: Array<Appender>
    )throws {
        breakDependencyWithParent()

        if let safeLevelString = dictionary[DictionaryKey.ThresholdLevel] as? String {
            if let safeLevel = Level(safeLevelString) {
                self.thresholdLevel = safeLevel
            } else {
                throw LoggerError.InvalidLevel
            }
        }

        if let appenderIds = dictionary[DictionaryKey.AppenderIds] as? Array<String> {
            appendersStorage.removeAll()
            for currentAppenderId in appenderIds {
                if let foundAppender = availableAppenders.find(filter: { $0.identifier == currentAppenderId }) {
                    appendersStorage.append(foundAppender)
                } else {
                    let messageError: String = "No such appender '\(currentAppenderId)' for logger \(self.identifier)"
                    throw LoggerError.NoSuchAppenderForLogger(messageError)
                }
            }
        }

        if let asynchronous = dictionary[DictionaryKey.Asynchronous] as? Bool {
            self.asynchronous = asynchronous
        }
    }

    func resetConfiguration() {
        self.thresholdLevel = .debug
        self.appenders = Logger.createDefaultAppenders()
        self.asynchronousStorage = false
    }

    @nonobjc public func trace(_ message: @autoclosure () -> Any,
        _ args: CVarArg..., file: String = #file,
        function: String = #function,
        line: Int = #line
    ) {
        let formattedMessage = "\(message())".formatter(args: args)
        self.log(level: .verbose, message: formattedMessage, file: file, function: function, line: line)
    }

    @nonobjc public func debug(_ message: @autoclosure () -> Any,
        _ args: CVarArg...,
        file: String = #file,
        function: String = #function,
        line: Int = #line
    ) {
        let formattedMessage = "\(message())".formatter(args: args)
        self.log(level: .debug, message: formattedMessage, file: file, function: function, line: line)
    }

    @nonobjc public func info(_ message: @autoclosure () -> Any,
        _ args: CVarArg...,
        file: String = #file,
        function: String = #function,
        line: Int = #line
    ) {
        let formattedMessage = "\(message())".formatter(args: args)
        self.log(level: .info, message: formattedMessage, file: file, function: function, line: line)
    }

    @nonobjc public func warn(_ message: @autoclosure () -> Any,
        _ args: CVarArg...,
        file: String = #file,
        function: String = #function,
        line: Int = #line
    ) {
        let formattedMessage = "\(message())".formatter(args: args)
        self.log(level: .warning, message: formattedMessage, file: file, function: function, line: line)
    }

    @nonobjc public func error(_ error: Error,
        _ args: CVarArg...,
        file: String = #file,
        function: String = #function,
        line: Int = #line
    ) {
        let message: String
        if let error = error as? IError {
            message = error.messageError(args)
        } else {
            message = error.localizedDescription
        }
        self.log(level: .error, message: message, file: file, function: function, line: line)
    }

    @nonobjc public func fatal(_ error: Error,
        _ args: CVarArg...,
        file: String = #file,
        function: String = #function,
        line: Int = #line
    ) {
        let message: String
        if let error = error as? IError {
            message = error.messageError(args)
        } else {
            message = error.localizedDescription
        }
        self.log(level: .fatal, message: message, file: file, function: function, line: line)
    }

    @nonobjc public func crash(_ error: Error,
        _ args: CVarArg...,
        file: String = #file,
        function: String = #function,
        line: Int = #line
    ) {
        let message: String
        if let error = error as? IError {
            message = error.messageError(args)
        } else {
            message = error.localizedDescription
        }
        self.log(level: .fatal, message: message, file: file, function: function, line: line)
        fatalError(message)
    }

    @nonobjc public func off(_ message: @autoclosure () -> Any,
        _ args: CVarArg...,
        file: String = #file,
        function: String = #function,
        line: Int = #line
    ) {
        let formattedMessage = "\(message())".formatter(args: args)
        self.log(level: .off, message: formattedMessage, file: file, function: function, line: line)
    }

    private func willIssueLogForLevel(_ level: Level) -> Bool {
        return level >= self.thresholdLevel && self.appenders.reduce(false) { (shouldLog, currentAppender) in
            shouldLog || level >= currentAppender.thresholdLevel
        }
    }

    private func log(level: Level, message: String,
        file: String? = #file,
        function: String? = #function,
        line: Int? = #line
    ) {
        if(self.willIssueLogForLevel(level)) {
            var info: LogInfoDictionary = [
                    .LoggerName: self.identifier,
                    .LogLevel: level,
                    .Timestamp: NSDate().timeIntervalSince1970,
                    .ThreadId: currentThreadId(),
                    .ThreadName: currentThreadName()
            ]
            if let file = file {
                info[.FileName] = file
            }
            if let line = line {
                info[.FileLine] = line
            }

            let logClosure = {
                var resolvedMessage: String?
                for currentAppender in self.appenders {
                    resolvedMessage = resolvedMessage == nil && currentAppender.hasMessageFilters() ? message : nil
                    if currentAppender
                        .shouldLevelBeLogged(level, path: file!, function: function!, message: resolvedMessage) {

                        let msgStr = resolvedMessage == nil ? message : resolvedMessage!
                        if let function = function {
                            let f = self.stripParams(function: function)
                            info[.Function] = f
                        }
                        currentAppender.log(msgStr, level: level, info: info)
                    }
                }
            }
            self.executeLogClosure(logClosure)
        }
    }

    private func stripParams(function: String) -> String {
        var f = function
        if let indexOfBrace = f.find("(") {
            #if swift(>=4.0)
                f = String(f[..<indexOfBrace])
            #else
                f = f.substring(to: indexOfBrace)
            #endif
        }
        f += "()"
        return f
    }

    private func executeLogClosure(_ logClosure: @escaping () -> ()) {
        if(self.asynchronous) {
            Logger.loggingQueue.async(execute: logClosure)
        } else {
            logClosure()
        }
    }

    private func breakDependencyWithParent() {
        guard let parent = self.parent else {
            return
        }
        self.thresholdLevelStorage = parent.thresholdLevel
        self.appendersStorage = parent.appenders
        self.parent = nil
    }

    private final class func createDefaultAppenders() -> [Appender] {
        return [/*ConsoleAppender("defaultAppender")*/]
    }
}

private func currentThreadName() -> String {
    if Thread.isMainThread {
        return "main"
    } else {
        var name: String = Thread.current.name ?? ""
        if name.isEmpty {
            let queueNameBytes = __dispatch_queue_get_label(nil)
            if let queuName = String(validatingUTF8: queueNameBytes) {
                name = queuName
            }
        }
        if name.isEmpty {
            name = String(format: "%p", Thread.current)
        }
        return name
    }
}

private func currentThreadId() -> UInt64 {
    var threadId: UInt64 = 0
    if (pthread_threadid_np(nil, &threadId) != 0) {
        threadId = UInt64(pthread_mach_thread_np(pthread_self()));
    }
    return threadId
}
