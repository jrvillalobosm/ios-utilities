//
//  LoggerBuilder.swift
//
//  Created by Jorge Villalobos on 09/02/19.
//  Copyright © 2019 Jorge Villalobos. All rights reserved.
//

import CommonsAssets
import Foundation

public class LoggerBuilder {
    private var appenders: [String: (Appender, String)] = [:]
    private var tokens: [String: ([String: AnyObject], String, LogInfoDictionary) -> String] = [:]
    private var rootLevel: Level = .debug
    private var logPattern: String = ConfigurationKey.DEFAULT_LOG_PATTERN
    private var onActionCustomAppender: (String, Level, LogInfoDictionary) -> () = { _, _, _ in }
    private var asynchronous: Bool = false

    public init() throws { }

    public func setRootLevel(rootLevel: Level) -> Self {
        self.rootLevel = rootLevel
        return self
    }

    public func setLogPatern(logPattern: String) -> Self {
        self.logPattern = logPattern
        return self
    }

    public func addResourceBundle(bundleId: String) throws -> Self {
        try BundleFacade.shared.addResourceBundle(bundleId: bundleId)
        return self
    }

    public func registerLogger(loggerName: String, level: Level = .debug, appenders: [Appender] = []) -> Self {
        let logger = Logger(identifier: loggerName, level: level, appenders: appenders)
        logger.thresholdLevel = level
        try! LoggerFactory.shared.registerLogger(logger)
        return self
    }

    public func addAppender(loggerName: String, appender: Appender) -> Self {
        let logger = LoggerFactory.shared.getLogger(loggerName)
        logger.appenders.append(appender)
        return self
    }

    public func resetConfiguration(resetConfiguration: Bool) -> Self {
        if(resetConfiguration) {
            LoggerFactory.shared.resetConfiguration()
        }
        return self
    }

    public func addConsoleAppender() -> Self {
        let consoleAppender = ConsoleAppender(ConfigurationKey.ConsoleAppender)
        self.appenders[ConfigurationKey.ConsoleAppender] = (consoleAppender, ConfigurationKey.ConsoleFormatter)
        return self
    }

    public func addCustomAppender(identifier: String,
        layoutId: String,
        _ onAction: @escaping (String, Level, LogInfoDictionary) -> ()
    ) -> Self {
        let customAppender = CustomAppender(identifier)
        customAppender.onAppend = onAction
        self.appenders[identifier] = (customAppender, layoutId)
        return self
    }

    public func addFilter(identifier: String, filter: FilterType) -> Self {
        if let item = self.appenders[identifier] {
            item.0.addFilter(filter)
        }
        return self
    }

    public func asynchronous(asynchronous: Bool) -> Self {
        self.asynchronous = asynchronous
        return self
    }

    public func addToken(tokenId: String,
        _ token: @escaping ([String: AnyObject], String, LogInfoDictionary) -> String
    ) -> Self {
        tokens[tokenId] = token
        return self
    }

    public func builder() throws {
        LoggerFactory.shared.rootLogger.thresholdLevel = rootLevel
        LoggerFactory.shared.rootLogger.asynchronous = asynchronous

        for item in self.appenders {
            try self.addRootAppender(layoutId: item.value.1, appender: item.value.0)
        }
    }

    private func addRootAppender(layoutId: String, appender: Appender) throws {
        let patternLyout = try PatternLayout(identifier: layoutId, pattern: self.logPattern, tokens: self.tokens)
        appender.thresholdLevel = self.rootLevel
        appender.formatter = patternLyout
        LoggerFactory.shared.rootLogger.appenders.append(appender)
    }
}
