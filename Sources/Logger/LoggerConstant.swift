//
//  LoggerConstant.swift
//
//  Created by Jorge Villalobos on 4/6/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import Foundation

public enum ConfigurationKey {
    static let DEFAULT_LOG_PATTERN = "[%l] %f.%M | %m"
    static let FirebaseAppender = "firebaseAppender"
    static let FirebaseFormatter = "firebaseFormatter"
    static let ConsoleAppender = "consoleAppender"
    static let ConsoleFormatter = "consoleFormatter"
}

public enum DictionaryKey {
    static let Pattern = "Pattern"
    static let ThresholdLevel = "ThresholdLevel"
    static let AppenderIds = "AppenderIds"
    static let Asynchronous = "Asynchronous"
    static let FormatterId = "FormatterId"
}

public enum LogInfoKeys {
    case LogLevel
    case LoggerName
    case FileName
    case FileLine
    case Function
    case Timestamp
    case ThreadId
    case ThreadName
}

public typealias LogInfoDictionary = Dictionary<LogInfoKeys, CustomStringConvertible>
