//
//  LoggerError.swift
//
//  Created by Jorge Villalobos on 4/6/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import CommonsAssets
import Foundation

public enum LoggerError: IError {
    case InvalidLoggerIdentifier
    case InvalidFormatSyntax
    case NotClosedMarkerParameter
    case MissingParameter
    case InvalidLevel
    case NoSuchAppenderForLogger(String)
    case InvalidFormatToJson(String)
    case NoSuchFormatterForAppender(String)

    public var code: Int {
        switch self {
        case .InvalidLoggerIdentifier:
            return 10001
        case .InvalidFormatSyntax:
            return 10002
        case .NotClosedMarkerParameter:
            return 10003
        case .MissingParameter:
            return 10004
        case .InvalidLevel:
            return 10005
        default:
            return 10006
        }
    }

    public var domain: String {
        return "Logger"
    }

    public func messageError(_ args: [CVarArg]) -> String {
        switch self {
        case .InvalidFormatToJson(let message),
             .NoSuchAppenderForLogger(let message),
             .NoSuchFormatterForAppender(let message):
            return message
        default:
            let message: String! = BundleFacade.shared.displayMessage(bundleId: bundleId, key: key, args: args)
            return message
        }
    }
}
