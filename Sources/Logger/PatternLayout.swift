//
//  PatternLayout.swift
//
//  Created by Jorge Villalobos on 4/6/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import Foundation

public protocol Layout {
    var identifier: String { get }

    init (_ identifier: String)

    func update(withDictionary dictionary: Dictionary<String, Any>,
        tokens: [String: ([String: AnyObject],
            String, LogInfoDictionary) -> String]) throws
    func format(message: String, info: LogInfoDictionary) -> String
}

@objc public final class PatternLayout: NSObject, Layout {
    public let identifier: String

    typealias FormattingClosure = (_ message: String, _ infos: LogInfoDictionary) -> String
    private var formattingClosuresSequence = [FormattingClosure]()

    public init(identifier: String,
        pattern: String,
        tokens: [String: ([String: AnyObject], String, LogInfoDictionary) -> String] = [:]
    ) throws {
        self.identifier = identifier
        let parser = PatternParser(tokens: tokens)
        super.init()
        self.formattingClosuresSequence = try parser.parsePattern(pattern)
    }

    public required convenience init(_ identifier: String) {
        try! self.init(identifier: identifier, pattern: "%m")
    }

    public func update(withDictionary dictionary: Dictionary<String, Any>,
        tokens: [String: ([String: AnyObject], String, LogInfoDictionary) -> String] = [:]
    ) throws {
        if let safePattern = (dictionary[DictionaryKey.Pattern] as? String) {
            let parser = PatternParser(tokens: tokens)
            self.formattingClosuresSequence = try parser.parsePattern(safePattern)
        } else {
            throw LoggerError.MissingParameter
        }
    }

    public func format(message: String, info: LogInfoDictionary) -> String {
        return formattingClosuresSequence.reduce("") {
            (accumulatedValue, currentItem) in accumulatedValue + currentItem(message, info)
        }
    }

    private class PatternParser {
        typealias MarkerClosure = (_ parameters: [String: AnyObject],
            _ message: String,
            _ info: LogInfoDictionary
        ) -> String

        private enum ParserState {
            case Text
            case Marker
            case PostMarker(String)
            case Parameters(String)
            case End
        }

        private struct ParserStatus {
            var machineState = ParserState.Text
            var charactersAccumulator = [Character]()

            func getParameterValues() throws -> [String: AnyObject] {
                do {
                    return try ("{" + String(charactersAccumulator) + "}").toJson()
                } catch let error {
                    throw LoggerError.InvalidFormatToJson(error.localizedDescription)
                }
            }

            func getToken() throws -> [String: AnyObject] {
                do {
                    return try ("{'token': '" + String(charactersAccumulator) + "'}").toJson()
                } catch let error {
                    throw LoggerError.InvalidFormatToJson(error.localizedDescription)
                }
            }
        }

        private var parserStatus = ParserStatus()
        private var parsedClosuresSequence = [FormattingClosure]()

        private var tokens: [String: ([String: AnyObject], String, LogInfoDictionary) -> String]

        init(tokens: [String: ([String: AnyObject], String, LogInfoDictionary) -> String]) {
            self.tokens = tokens
        }

        fileprivate func parsePattern(_ pattern: String) throws -> [FormattingClosure] {
            parsedClosuresSequence = [FormattingClosure]()

            for currentCharacter in pattern {
                switch(parserStatus.machineState) {
                case .Text where currentCharacter == "%":
                    try setParserState(.Marker)
                case .Text:
                    parserStatus.charactersAccumulator.append(currentCharacter)
                case .Marker:
                    try setParserState(.PostMarker(String(currentCharacter)))
                case .PostMarker(let markerName) where currentCharacter == "{":
                    try setParserState(.Parameters(markerName))
                case .PostMarker:
                    try setParserState(.Text)
                    parserStatus.charactersAccumulator.append(currentCharacter)
                case .Parameters where currentCharacter == "}":
                    try setParserState(.Text)
                case .Parameters:
                    parserStatus.charactersAccumulator.append(currentCharacter)
                case .End:
                    throw LoggerError.InvalidFormatSyntax
                }
            }
            try setParserState(.End)
            return parsedClosuresSequence
        }

        private func setParserState(_ newState: ParserState) throws {
            switch(parserStatus.machineState) {
            case .Text where parserStatus.charactersAccumulator.count > 0:
                let parsedString = String(parserStatus.charactersAccumulator)
                if(!parsedString.isEmpty) {
                    parsedClosuresSequence.append({ (_, _) in return parsedString })
                    parserStatus.charactersAccumulator.removeAll()
                }
            case .PostMarker(let markerName):
                switch(newState) {
                case .Text, .End:
                    parserStatus.charactersAccumulator.removeAll()
                    processMarker(markerName)
                case .Parameters:
                    break
                default:
                    break
                }
            case .Parameters(let markerName):
                switch(newState) {
                case .End:
                    throw LoggerError.NotClosedMarkerParameter
                default:
                    do {
                        switch markerName {
                        case "X":
                            try processMarker(markerName, parameters: parserStatus.getToken())
                        default:
                            try processMarker(markerName, parameters: parserStatus.getParameterValues())
                        }
                    }
                    catch {
                        throw LoggerError.InvalidFormatSyntax
                    }
                    parserStatus.charactersAccumulator.removeAll()
                }
            default:
                break
            }
            parserStatus.machineState = newState
        }

        private func processMarker(_ markerName: String, parameters: [String: AnyObject] = [:]) {
            if let closureForMarker = self.closureForMarker(markerName, parameters: parameters) {
                parsedClosuresSequence.append({ (message, info) in closureForMarker(parameters, message, info) })
            } else {
                parserStatus.charactersAccumulator += "%\(markerName)"
            }
        }

        private func closureForMarker(_ marker: String, parameters: [String: AnyObject]) -> MarkerClosure? {
            let generatedClosure: MarkerClosure?

            switch(marker) {
            case "d":
                generatedClosure = { (parameters, message, info) in
                    let result: String
                    let format = parameters["format"] as? String ?? "%F %T"
                    let timestamp = info[.Timestamp] as? TimeInterval ?? NSDate().timeIntervalSince1970
                    var secondsSinceEpoch = Int(timestamp)
                    let date = withUnsafePointer(to: &secondsSinceEpoch) {
                        localtime($0)
                    }
                    let buffer = UnsafeMutablePointer<Int8>.allocate(capacity: 80)
                    strftime(buffer, 80, format, date)
                    result = String(bytesNoCopy: buffer, length: strlen(buffer), encoding: .utf8, freeWhenDone: true)
                        ?? "error"

                    return processCommonParameters(result, parameters: parameters)
                }
            case "D":
                let format = parameters["format"] as? String ?? "yyyy-MM-dd HH:mm:ss.SSS"
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = format
                generatedClosure = { (parameters, message, info) in
                    let result: String
                    let date: Date
                    if let timestamp = info[.Timestamp] as? Double {
                        date = Date(timeIntervalSince1970: timestamp)
                    } else {
                        date = Date()
                    }
                    result = dateFormatter.string(from: date)
                    return processCommonParameters(result, parameters: parameters)
                }
            case "l":
                generatedClosure = { (parameters, message, info) in
                    let logLevel = info[.LogLevel] ?? "-"
                    return processCommonParameters(logLevel, parameters: parameters)
                }
            case "n":
                generatedClosure = { (parameters, message, info) in
                    let loggerName = info[.LoggerName] ?? "-"
                    return processCommonParameters(loggerName, parameters: parameters)
                }
            case "L":
                generatedClosure = { (parameters, message, info) in
                    let line = info[.FileLine] ?? "-"
                    return processCommonParameters(line, parameters: parameters)
                }
            case "F":
                generatedClosure = { (parameters, message, info) in
                    let filename = info[.FileName] ?? "-"
                    return processCommonParameters(filename, parameters: parameters)
                }
            case "f":
                generatedClosure = { (parameters, message, info) in
                    let filename = self.fileNameWithoutSuffix((info[.FileName] as? String)!)
                    return processCommonParameters(filename, parameters: parameters)
                }
            case "M":
                generatedClosure = { (parameters, message, info) in
                    let function = info[.Function] ?? "-"
                    return processCommonParameters(function, parameters: parameters)
                }
            case "t":
                generatedClosure = { (parameters, message, info) in
                    let threadId: String
                    if let tid = info[.ThreadId] as? UInt64 {
                        threadId = String(tid, radix: 16, uppercase: false)
                    } else {
                        threadId = "-"
                    }
                    return processCommonParameters(threadId, parameters: parameters)
                }
            case "T":
                generatedClosure = { (parameters, message, info) in
                    let threadName = info[.ThreadName] ?? "-"
                    return processCommonParameters(threadName, parameters: parameters)
                }
            case "m":
                generatedClosure = { (parameters, message, info) in
                    processCommonParameters(message as String, parameters: parameters)
                }
            case "X":
                guard let token = parameters["token"] as? String else {
                    return nil
                }
                guard let closure = tokens[token] else {
                    return { (parameters, message, info) in
                        let result: String = "Undefined Config"
                        return processCommonParameters(result, parameters: parameters)
                    }
                }
                generatedClosure = { (parameters, message, info) in
                    let result: String = closure(parameters, message, info)
                    return processCommonParameters(result, parameters: parameters)
                }

            case "%":
                generatedClosure = { (parameters, message, info) in "%" }
            default:
                generatedClosure = nil
            }
            return generatedClosure
        }

        private func fileNameOfFile(_ file: String) -> String {
            let fileParts = file.components(separatedBy: "/")
            if let lastPart = fileParts.last {
                return lastPart
            }
            return ""
        }

        private func fileNameWithoutSuffix(_ file: String) -> String {
            let fileName = self.fileNameOfFile(file)

            if !fileName.isEmpty {
                let fileNameParts = fileName.components(separatedBy: ".")
                if let firstPart = fileNameParts.first {
                    return firstPart
                }
            }
            return ""
        }
    }
}

func processCommonParameters(_ value: CustomStringConvertible, parameters: [String: AnyObject]) -> String {
    var width: Int = 0

    if let widthString = parameters["padding"] as? NSString {
        width = widthString.integerValue
    }

    return value.description.pad(toWidth: width)
}
