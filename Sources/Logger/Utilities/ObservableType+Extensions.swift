//
//  ObservableType+Extensions.swift
//
//  Created by Jorge Villalobos on 29/05/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import CommonsAssets
import Foundation
import RxSwift

public extension ObservableType {
    func selector(transform: @escaping (Element?, Error?) -> ()) -> Observable<Self.Element> {
        return Observable.create { observer in
            let subscription = self.subscribe { e in
                switch e {
                case .next(let value):
                    transform(value, nil)
                    observer.on(.next(value))
                case .error(let error):
                    transform(nil, error)
                    observer.on(.error(error))
                case .completed:
                    observer.on(.completed)
                }
            }
            return subscription
        }
    }

    func debugging(onNext: String, onError: String) -> Observable<Self.Element> {
        return Observable.create { observer in
            let subscription = self.subscribe { e in
                switch e {
                case .next(let value):
                    Logger.getLogger("Debugging").debug("[Value : \(value)] ==> \(onNext)")
                    observer.on(.next(value))
                case .error(let error as IError):
                    Logger.getLogger("Debugging").debug("[Error : \(error.messageError())] ==> \(onError)")
                    observer.on(.error(error))
                case .error(let error):
                    Logger.getLogger("Debugging").debug("[Error : \(error.localizedDescription)] ==> \(onError)")
                    observer.on(.error(error))
                case .completed:
                    observer.on(.completed)
                }
            }
            return Disposables.create {
                subscription.dispose()
            }
        }
    }

    func debuggingOnNext(identifier: String) -> Observable<Self.Element> {
        return Observable.create { observer in
            let subscription = self.subscribe { e in
                switch e {
                case .next(let value):
                    Logger.getLogger("Debugging").debug("[Value : \(value)] ==> \(identifier)")
                    observer.on(.next(value))
                case .error(let error):
                    observer.on(.error(error))
                case .completed:
                    observer.on(.completed)
                }
            }
            return Disposables.create {
                subscription.dispose()
            }
        }
    }

    func debuggingOnError(identifier: String) -> Observable<Self.Element> {
        return Observable.create { observer in
            let subscription = self.subscribe { e in
                switch e {
                case .next(let value):
                    observer.on(.next(value))
                case .error(let error as IError):
                    Logger.getLogger("Debugging").debug("[Error : \(error.messageError())] ==> \(identifier)")
                    observer.on(.error(error))
                case .error(let error):
                    Logger.getLogger("Debugging").debug("[Error : \(error.localizedDescription)] ==> \(identifier)")
                    observer.on(.error(error))
                case .completed:
                    observer.on(.completed)
                }
            }
            return Disposables.create {
                subscription.dispose()
            }
        }
    }
}
