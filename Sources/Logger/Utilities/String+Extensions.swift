//
//  String+Extensions.swift
//
//  Created by Jorge Villalobos on 07/02/19.
//  Copyright © 2019 Jorge Villalobos. All rights reserved.
//

import Foundation
import UIKit

extension StringProtocol {
    public func stringByRemovingLastComponent(withDelimiter delimiter: String) -> SubSequence? {
        guard let markerIndex = self.reversed().firstIndex(of: Character(delimiter)) else { return nil }
        let endIndex = self.index(markerIndex.base, offsetBy: -1)

        let result = self[self.startIndex..<endIndex]
        return result
    }
}

public extension String {
    func pad(toWidth width: Int) -> String {
        var paddedString: String = self

        if width == 0 {
            return self
        }

        if self.count > abs(width) {
            if width < 0 {
                paddedString = String(self.suffix(abs(width)))
            } else {
                paddedString = String(self.prefix(width))
            }
        }

        if self.count < abs(width) {
            if width < 0 {
                paddedString = " ".padding(toLength: abs(width) - self.count, withPad: " ", startingAt: 0) + self
            } else {
                paddedString = self.padding(toLength: width, withPad: " ", startingAt: 0)
            }
        }

        return paddedString
    }

    func find(_ char: Character) -> Index? {
        #if swift(>=3.2)
            return self.firstIndex(of: char)
        #else
            return self.characters.index(of: char)
        #endif
    }
}
