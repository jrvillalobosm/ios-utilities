//
//  CTKFlagPhoneNumberDelegate.swift
//
//  Created by Jorge Villalobos on 30/08/18.
//  Copyright © 2018 Jorge Villalobos. All rights reserved.
//

import Foundation

protocol CTKFlagPhoneNumberDelegate {
    func didSelect(country: Country)
}
