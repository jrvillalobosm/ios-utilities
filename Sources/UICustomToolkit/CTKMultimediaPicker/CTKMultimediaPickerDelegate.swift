//
//  CTKMultimediaPickerDelegate.swift
//
//  Created by Jorge Villalobos on 05/09/18.
//  Copyright © 2018 Jorge Villalobos. All rights reserved.
//

import Foundation
import UIKit

public protocol CTKMultimediaPickerDelegate {
    func mediaPickerController(url: URL,
        fileName: String,
        data: Data,
        editedImage: UIImage?,
        contentType: String,
        type: MultimediaType)
    func presentPicker(byImage picker: UIImagePickerController)
    func presentPicker(byDocument picker: UIDocumentPickerViewController)
    func dismissPicker(byImage picker: UIImagePickerController, wasCancelled: Bool)
    func dismissPicker(byDocument picker: UIViewController, wasCancelled: Bool)
    func presentAlertController(alertController: UIAlertController, successful: Bool)
}

public extension CTKMultimediaPickerDelegate where Self: UIViewController {
    func presentPicker(byImage picker: UIImagePickerController) {
        DispatchQueue.main.async {
            self.present(picker, animated: true, completion: nil)
        }
    }

    func dismissPicker(byImage picker: UIImagePickerController, wasCancelled: Bool) {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }

    func presentPicker(byDocument picker: UIDocumentPickerViewController) {
        DispatchQueue.main.async {
            self.present(picker, animated: true, completion: nil)
        }
    }

    func dismissPicker(byDocument picker: UIViewController, wasCancelled: Bool) {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }

    func presentAlertController(alertController: UIAlertController, successful: Bool) {
        DispatchQueue.main.async {
            self.present(alertController, animated: true)
        }
    }
}
