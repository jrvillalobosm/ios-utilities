//
//  Bundle+Extension.swift
//
//  Created by Jorge Villalobos on 30/08/18.
//  Copyright © 2018 Jorge Villalobos. All rights reserved.
//

import Foundation

public extension Bundle {
    static var UICustomToolkitIcons = UICustomToolkit()

    static func UICustomToolkit() -> Bundle {
        return Bundle.main
        /*
        let bundle = Bundle(for: CTKFlagPhoneNumberTextField.self)
        if let path = bundle.path(forResource: "UICustomToolkit", ofType: "bundle") {
            return Bundle(path: path)!
        } else {
            return bundle
        }
        // */
    }

    static func CTKFlagPhoneNumber() -> Bundle {
        let bundle = Bundle(for: CTKFlagPhoneNumberTextField.self)
        if let path = bundle.path(forResource: "CTKFlagPhoneNumber", ofType: "bundle") {
            return Bundle(path: path)!
        } else {
            return bundle
        }
    }

    static func CTKMultimediaPicker() -> Bundle {
        let bundle = Bundle(for: CTKMultimediaPickerController.self)
        if let path = bundle.path(forResource: "CTKMultimediaPicker", ofType: "bundle") {
            return Bundle(path: path)!
        } else {
            return bundle
        }
    }
}

